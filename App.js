import React, { Component } from 'react';

import {
  Text,
  View,
  StyleSheet,
} from 'react-native';

import {
  COLOR,
  Button,
  getTheme,
  Checkbox,
  ThemeContext,
} from 'react-native-material-ui';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  text: {
    color: 'black',
    fontSize: 25,
  },
});


const uiTheme = {
  palette: {
    primaryColor: COLOR.red500,
  },
  toolbar: {
    container: {
      height: 50,
    },
  },
};

export default class extends Component {
  render() {
    return (
      <ThemeContext.Provider value={getTheme(uiTheme)}>
        <View style={styles.container}>
          <Text style={styles.text}>Welcome to React Native!</Text>

          <Button
            primary
            raised
            text="Primary"
          />
        </View>
      </ThemeContext.Provider>
    );
  }
}
